# Changelog

## [0.2] 2020-12-10
### Added
- Agregado soporte para múltiples recibos de nómina en un mismo CFDI. Ahora 
  `cfdi.complemento.nomina` puede un objeto `Nomina` o una lista de nóminas 
  `[Nomina, ...]`.