
run_tests:
    stage: test
    image: python:3
    before_script:
        - pip install -r requirements.txt
    script:
        - pytest -v

pypi-release:
  image: python:3

  before_script:
    # Check required variables are set. This should be enforced by the rules
    # below but we want to make sure that they are actually set in the
    # environment as well.
    - '[ -z "${PYPI_API_TOKEN}" ] && (echo "PYPI_API_TOKEN variable must be set" >&2; exit 1)'

  script:
    # Install required utilities
    - pip3 install twine

    - |-
      if [[ -z "$DIST_LOCATION" ]]; then
        export DIST_LOCATION=dist
        # Build a source distribution
        python3 setup.py sdist bdist_wheel
      fi

      echo "Using dist location $DIST_LOCATION"

      # Show the contents of the dist directory to aid debugging.
      ls -lR $DIST_LOCATION

    # Upload using twine
    - 'echo "Using the following additional twine options: $TWINE_OPTS"'
    - TWINE_PASSWORD=$PYPI_API_TOKEN twine upload
        --disable-progress-bar
        --verbose
        --non-interactive
        --username __token__
        $TWINE_OPTS
        "${DIST_LOCATION}/*"

  rules:
    # Don't create a detached job on a merge request - a separate pipeline
    # should be created for a branch which is where we expect this to be run.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    # Releasing to PyPI should not be an automatic process. Make it a manually
    # triggered job. Only allow a manual run if required variables set.
    - if: "$PYPI_API_TOKEN"
      when: manual

  # As a stand alone job, this does not "need" any job to have completed first.
  needs: []

  # As a stand alone job, this could be in any stage but "deploy" makes a degree
  # of sense to a human.
  stage: deploy

# A job which can be used to upload a packaged release to the test PyPI instance
# instead. Acts like pypi-release except that it requires that the
# TEST_PYPI_API_TOKEN variable be set in place of PYPI_API_TOKEN.
test-pypi-release:
  extends: pypi-release

  variables:
    # Map TEST_PYPI_API_TOKEN to the PYPI_API_TOKEN variable.
    PYPI_API_TOKEN: "$TEST_PYPI_API_TOKEN"

    # Use the testpypi environment for upload
    TWINE_OPTS: "--repository-url https://upload.pypi.org/legacy/"

  rules:
    # Don't create a detached job on a merge request - a separate pipeline
    # should be created for a branch which is where we expect this to be run.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    # Releasing to PyPI should not be an automatic process. Make it a manually
    # triggered job. Only allow a manual run if required variables set.
    - if: "$TEST_PYPI_API_TOKEN"
      when: manual
